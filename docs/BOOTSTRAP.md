This file describes how to bootstrap the harbor production cluster from a critical base service failure

## Scenarios
### Bootstrap without magnum image and helm dependencies
To bring up the registry cluster without having a working registry (as magnum needs the registry's images and helm charts) we need to point the origin of the containers to an external source by means of `container_infra_prefix`. The helm charts will have to be installed manually from the repository.

Currently, we can use:
- container_infra_prefix=gitlab-registry.cern.ch/cloud/atomic-system-containers/
- TODO: ongoing work to push critical images to github. See https://its.cern.ch/jira/browse/OS-15849

To bring up the cluster use:
```bash
openstack coe cluster create cci-infra-ha-001 \
--keypair dtomasgu-key \
--cluster-template kubernetes-1.22.3-4-multi \
--master-flavor m2.medium \
--master-count 3 \
--flavor m2.large \
--node-count 1 \
--merge-labels \
--labels cvmfs_enabled=false \
--labels eos_enabled=false \
--labels monitoring_enabled=true \
--labels max_node_count=10 \
--labels ingress_controller=nginx \
--labels logging_producer=kubernetes \
--labels container_infra_prefix=gitlab-registry.cern.ch/cloud/atomic-system-containers/
```

After the cluster is up, install the helm cern-magnum chart manually by following:
```bash
git clone https://gitlab.cern.ch/kubernetes/automation/releases/cern-magnum.git
cd cern-magnum
sed -i 's@registry.cern.ch/chartrepo@YOURGITHUBREPO@g' Chart.yaml
helm dependency build
helm upgrade -i cern-magnum ../cern-magnum -n kube-system --set base.enabled=false --set eosxd.enabled=false --set nvidia-gpu.enabled=false --set fluentd.producer=kubernetes --set openstack-cinder-csi.enabled=false --set cvmfs-csi.enabled=false --set ingress-nginx.enabled=true --set traefik.enabled=false --set kube-prometheus-stack.enabled=true
```

You also need to bring up the PVCs without using the Networked Storage (Manila).
For this, you can use the manifest available at static/bootstrap-{staging,prod}

For the next steps, follow the README.md landing page of this project https://gitlab.cern.ch/kubernetes/automation/releases/cci-container-infra
